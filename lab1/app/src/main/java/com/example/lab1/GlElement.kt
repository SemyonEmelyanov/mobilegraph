package com.example.lab1

import android.opengl.GLES20
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import java.nio.ShortBuffer


class GlElement(var coordsPerVertex: Int = 0,var coordinates: FloatArray, var  color: FloatArray, var drawOrder:ShortArray   ) {
    private val mProgram by lazy{
        GLES20.glCreateProgram()
    }
    private var vertexBuffer: FloatBuffer? = null
    private var mPositionHandle = 0
    private var mColorHandle = 0
    private var mMVPMatrixHandle = 0
    private var drawListBuffer: ShortBuffer? = null
    var coords = floatArrayOf()
    private var vertexCount = 0
    private var vertexStride = 0



    init {
        vertexCount = coordinates.size / coordsPerVertex
        vertexStride = coordsPerVertex * 4
        val bb: ByteBuffer =
            ByteBuffer.allocateDirect(
                coords.size * 4
            )
        bb.order(ByteOrder.nativeOrder())
        vertexBuffer = bb.asFloatBuffer()
        vertexBuffer!!.put(coords)
        vertexBuffer!!.position(0)
        val dlb: ByteBuffer =
            ByteBuffer.allocateDirect(
                drawOrder.size * 2
            )
        dlb.order(ByteOrder.nativeOrder())
        drawListBuffer = dlb.asShortBuffer()
        drawListBuffer!!.put(drawOrder)
        drawListBuffer!!.position(0)

        val vertexShader: Int = MyGlSurfaceRender.loadShader(
            GLES20.GL_VERTEX_SHADER,
            MySimpleOpenGLES2DrawingClass.vertexShaderCode
        )
        val fragmentShader: Int = MyGlSurfaceRender.loadShader(
            GLES20.GL_FRAGMENT_SHADER,
            MySimpleOpenGLES2DrawingClass.fragmentShaderCode
        )
        GLES20.glAttachShader(mProgram, vertexShader )
        GLES20.glAttachShader(mProgram, fragmentShader)
        GLES20.glLinkProgram(mProgram)
    }

    fun draw(mvpMatrix: FloatArray?,drawMode: Int = GLES20.GL_TRIANGLES) {
        GLES20.glUseProgram(mProgram)
        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition")
        GLES20.glEnableVertexAttribArray(mPositionHandle)
        GLES20.glVertexAttribPointer(
            mPositionHandle, coordsPerVertex,
            GLES20.GL_FLOAT, false,
            vertexStride, vertexBuffer
        )
        mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor")
        GLES20.glUniform4fv(mColorHandle, 1, color, 0)
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix")
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0)
        GLES20.glDrawElements(drawMode, drawOrder.size, GLES20.GL_UNSIGNED_SHORT, drawListBuffer)
        GLES20.glDisableVertexAttribArray(mPositionHandle)
    }





}
package com.example.lab1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.opengl.GLSurfaceView
import android.view.Window
import android.view.WindowManager
import com.example.lab1.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

//    private val mGLView: GLSurfaceView by lazy {MyGlSurfaceView(this)}
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}
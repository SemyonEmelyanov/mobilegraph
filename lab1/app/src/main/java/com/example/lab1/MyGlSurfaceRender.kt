package com.example.lab1

import android.opengl.GLSurfaceView
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10
import android.opengl.GLES20
import android.opengl.Matrix
import android.util.Log
import java.lang.RuntimeException


class MyGlSurfaceRender: GLSurfaceView.Renderer {
    private val mMVPMatrix = FloatArray(16)
    private val mProjectionMatrix = FloatArray(16)
    private val mViewMatrix = FloatArray(16)

    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {

        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    }

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        GLES20.glViewport(0, 0, width, height)
        val ratio = width.toFloat() / height
        Matrix.frustumM(mProjectionMatrix, 0, -ratio, ratio, -1F, 1F, 3F, 7F)
    }

    override fun onDrawFrame(gl: GL10?) {
        val scratch = FloatArray(16)
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT or GLES20.GL_DEPTH_BUFFER_BIT)
        Matrix.setLookAtM(mViewMatrix, 0, 0f, 0f, -3f, 0f, 0f, 0f, 0f, 1.0f, 0.0f)
        Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0)
        GlElement(
            3,
            floatArrayOf(
                -0.5f,
                0.5f,
                0.0f,
                -0.5f,
                -0.5f,
                0.0f,
                -0.49f,
                -0.5f,
                0.0f,
                -0.49f,
                0.5f,
                0.0f
            ),
            floatArrayOf(1.0f, 0.0f, 0.0f, 1.0f),
            shortArrayOf(0, 1, 2, 0, 2, 3)
        ).draw(mMVPMatrix)
        // Rectangle Demo..............................
        // Rectangle Demo..............................
        GlElement(
            3,
            floatArrayOf(-1.0f, 0.5f, 0.0f, -1.0f, 1.0f, 0.0f, 0f, 1.0f, 0.0f, 0f, .5f, 0.0f),
            floatArrayOf(0.0f, 1.5f, 0.0f, 1.0f),
            shortArrayOf(0, 1, 2, 0, 2, 3)
        ).draw(mMVPMatrix)
        //Triangle..............................
        //Triangle..............................
        GlElement(
            3,
            floatArrayOf(0.9f, 0.7f, 0.0f, .9f, .2f, 0.0f, .4f, .2f, 0.0f),
            floatArrayOf(0.0f, 0.0f, 1.0f, 1.0f),
            shortArrayOf(0, 1, 2)
        ).draw(mMVPMatrix)
    }











    companion object{
        const val TAG = "MyGlSurfaceRender"
        fun loadShader(type: Int, shaderCode: String?): Int {
            val shader = GLES20.glCreateShader(type)
            GLES20.glShaderSource(shader, shaderCode)
            GLES20.glCompileShader(shader)
            return shader
        }
        fun checkGlError(glOperation: String) {
            var error: Int
            while (GLES20.glGetError().also { error = it } != GLES20.GL_NO_ERROR) {
                Log.e(TAG, "$glOperation: glError $error")
                throw RuntimeException("$glOperation: glError $error")
            }
        }
    }
}
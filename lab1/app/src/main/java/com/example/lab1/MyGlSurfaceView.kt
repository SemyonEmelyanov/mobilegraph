package com.example.lab1

import android.annotation.SuppressLint
import android.content.Context
import android.opengl.GLSurfaceView
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import androidx.annotation.Nullable


class MyGlSurfaceView: GLSurfaceView {
    private var mRenderer: MyGlSurfaceRender? = null
    constructor(context: Context) : super(context) {
        setEGLContextClientVersion(2)
        mRenderer = MyGlSurfaceRender()
        setRenderer(mRenderer)
        renderMode = RENDERMODE_WHEN_DIRTY

    }
    constructor(context: Context, @Nullable  attrs: AttributeSet): super(context,attrs) {
        setEGLContextClientVersion(2)
        mRenderer = MyGlSurfaceRender()
        setRenderer(mRenderer)
        renderMode = RENDERMODE_WHEN_DIRTY
    }

    constructor(context: Context, @Nullable  attrs: AttributeSet,defStyleAttr: Int): super(context,attrs) {
        setEGLContextClientVersion(2)
        mRenderer = MyGlSurfaceRender()
        setRenderer(mRenderer)
        renderMode = RENDERMODE_WHEN_DIRTY
    }



    override fun onTouchEvent(e: MotionEvent): Boolean {
        val x = e.x
        val y = e.y
        Log.i("GL Surface View", "X=$x Y=$y")
        return false
    }

}